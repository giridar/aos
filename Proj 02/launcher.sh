#!/bin/bash
config="$1"
user="$2"
service="$3"
nodes=()
i=0
# Read the file given in argument line by line
while IFS= read -r line || [ -n "$line" ]; do
	# Skip lines that do not start with an unsigned integer
	if [[ ! $line =~ ^[0-9]+ ]]; then
		continue
	fi

	# Truncate comments after '#' and leading white spaces
	tokens=($(echo ${line%%#*} | sed -e 's/^ *//g;s/ *$//g'))

	# Read config parameters
	if [[ $i -eq 0 ]]; then
		global_info=("${tokens[@]}")
	elif [[ $i -le ${global_info[0]} ]]; then
		nodes[((${tokens[0]}))]="${tokens[@]}"
	fi
	((i+=1))
done < "$config"

bin_dir="bin"
bin_file="NodeRunner"
for (( i=${#nodes[@]}-1; i>=0; i-- )); do
	node=(${nodes[$i]})
	echo Launching Node ${node[0]} on ${node[1]} as $user...
	if [[ $i -gt 0 ]]; then
		# Run normal nodes in background '&'
		ssh $user@${node[1]} "java -cp workspace/AOS\ P02/$bin_dir $bin_file ${node[0]} workspace/AOS\ P02/$config $service > ${node[0]}.log 2>&1 &"
	else
		# Run the Initiator Node in forground to know termination of the processes
		ssh $user@${node[1]} "java -cp workspace/AOS\ P02/$bin_dir $bin_file ${node[0]} workspace/AOS\ P02/$config $service > ${node[0]}.log 2>&1"
	fi
	echo
done
