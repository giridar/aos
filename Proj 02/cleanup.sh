#!/bin/bash
config="$1"
user="$2"
nodes=()
i=0
# Read the file given in argument line by line
while IFS= read -r line || [ -n "$line" ]; do
	# Skip lines that do not start with an unsigned integer
	if [[ ! $line =~ ^[0-9]+ ]]; then
		continue
	fi

	# Truncate comments after '#' and leading white spaces
	tokens=($(echo ${line%%#*} | sed -e 's/^ *//g;s/ *$//g'))

	# Read config parameters
	if [[ $i -eq 0 ]]; then
		global_info=("${tokens[@]}")
	elif [[ $i -le ${global_info[0]} ]]; then
		nodes[((${tokens[0]}))]="${tokens[@]}"
	fi
	((i+=1))
done < "$config"

output_dir="output"
bin_file="NodeRunner"
result="result.txt"
msg_count=0
exec_time=0
resp_time=0
for node in "${nodes[@]}"; do
	# Copy all output files & delete the remote directory 
	node=($node)
	echo Cleaning up Node ${node[0]} on ${node[1]}...
	ssh $user@${node[1]} "pkill -u $user $bin_file"
	scp $user@${node[1]}:./${node[0]}.* $output_dir/.
	ssh $user@${node[1]} "rm -f ${node[0]}.*"

	read -r line < "$output_dir/${node[0]}.out"
	line=($line)
	msg_count=$(($msg_count + ${line[0]}))
	resp_time=$(($resp_time + ${line[1]}))
	if [[ ${line[1]} -gt $exec_time ]]; then
		exec_time="${line[2]}"
	fi
	echo
done
resp_time=$(($resp_time / ${#nodes[@]}))
echo $msg_count $resp_time $exec_time
echo "$msg_count,$resp_time,$exec_time" > "$result"
