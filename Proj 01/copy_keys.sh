#!/bin/bash
cd ~
#ssh-keygen
#ssh-add
user=$1
for (( i=1; i<=45; i++ )); do
	if [[ i -lt 10 ]]; then
		scp .ssh/id_rsa.pub $user@dc0$i.utdallas.edu:~/.ssh/authorized_keys
	else
		scp .ssh/id_rsa.pub $user@dc$i.utdallas.edu:~/.ssh/authorized_keys
	fi
done
