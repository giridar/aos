#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <string.h>
#include <pthread.h>

#define INITIATOR_NODE 0 		/* The node that is ACTIVE at the beginning */
#define MAX_BUFFER 100			/* Maximum size of the application/control messages */
#define CONN_DELAY 5			/* Initial wait delay for all processes to setup before starting connections */
#define MAX(X,Y) (X > Y)? X : Y/* Find the max of 2 numbers */

/* Different processing states of the node */
enum process_state {
	ACTIVE,						/* Currently sending Application msgs */
	PASSIVE,					/* Temporarily inactive after sending Application msgs */
	DONE						/* The Node has received FINISH msg from parent */
};
typedef enum process_state PROCESS_STATE;

/* Different snapshot states of the node */
enum snapshot_state {
	IN_PROGRESS,				/* A snapshot is currently in progress */
	COMPLETED					/* No snapshot in progress */
};
typedef enum snapshot_state SNAPSHOT_STATE;

/* Details of an upstream/downstream neighbor */
struct node {
	int id;						/* Node identification number */
	int in;						/* Socket to receive msgs from the node */
	int out;					/* Socket to send msgs to the node */
	int cState;					/* Channel state of the neighbor node */
	SNAPSHOT_STATE sState;		/* Monitoring state of the channel from neighbor */
};
typedef struct node NODE;

/* Different types of messages exchanged between the nodes */
enum msg_type {
	IDENTIFICATION,				/* Empty Handshake msg to initially exchange NodeId to neighbor */
	APPLICATION,				/* Application msg with just the vector clock of the sender */
	MARKER,						/* Empty Control msg sent by the Snapshot Protocol to start & end a channel monitoring */
	SNAPSHOT,					/* Control msg containing the local state of a node */
	FINISH						/* Empty Control msg to denote the termination of the application */
};
typedef enum msg_type MSG_TYPE;

/* Common Structure for all kinds of messages */
struct msg {
	int from, to, buffer[MAX_BUFFER], size;
	MSG_TYPE type;
};
typedef struct msg MSG;

/* Pre-set properties of a Node */
int nodeId, port, nodesCount, minPerActive, maxPerActive, maxNumber, neighborsCount, sentCount, parent, *vectorClock, *localState, **globalState, pendingMarkers, pendingSnapshots;
float minSendDelay, snapshotDelay;
char **args;					/* Input arguments for the node */
NODE *neighbors;				/* Details of neighboring nodes */
PROCESS_STATE pState;			/* Processing state of the node */
SNAPSHOT_STATE sState;			/* Snapshot state of the node */
pthread_t lTId, cTId, *inTId, outTId, sTId;	/* Thread Ids to keep track of receiver, sender & snapshot threads */
pthread_mutex_t pStateLock;		/* Lock to access ProcessState, SentCount & VectorClock */
pthread_mutex_t  sStateLock;	/* Lock to access SnapshotState, LocalState, PendingMarkers & PendingSnapshots */
FILE *outputFile;				/* Output file to record the local states in */

void die(char* cause) {
	/* Terminate the process on error */
	perror(cause);
	exit(1);
}

void print_array(int a[], int size) {
	printf("[");
	for(int i=0; i<size; i++)
		printf("%d ", a[i]);
	printf("]\n");
}

void print_clock() {
	/* Print the vector clock of the node. Make sure the clock is locked before calling this function */
	printf("Clock: ");
	print_array(vectorClock, nodesCount);
}

void print_msg(MSG m) {
	/* Print the contents of a msg */
	printf("Node %d to Node %d: ", m.from, m.to);
	switch(m.type) {
		case IDENTIFICATION:
		printf("ID");
		break;

		case APPLICATION:
		printf("APP");
		break;

		case MARKER:
		printf("MRK");
		break;

		case SNAPSHOT:
		printf("SNP");
		break;

		case FINISH:
		printf("FIN");
		break;
	}
	printf(" - ");
	print_array(m.buffer, m.size);
}

void *send_app_msgs() {
	/* Send MAP application messages */
	int range = maxPerActive - minPerActive;
	int n = (int) (((double)(range+1)/RAND_MAX) * rand() + minPerActive);
	MSG m;
	m.type = APPLICATION;
	m.from = nodeId;
	m.size = nodesCount;
	printf("Sending %d Application msgs...\n", n);
	for(int i=0; i<n; i++) {
		int r = (int) (((double)(neighborsCount)/RAND_MAX) * rand());
		pthread_mutex_lock(&pStateLock);

		m.to = neighbors[r].id;
		memcpy(m.buffer, vectorClock, nodesCount * sizeof(int));
		sctp_sendmsg(neighbors[r].out, &m, sizeof(m), NULL, 0, 0, 0, 0 ,0, 0);
		print_msg(m);
		vectorClock[nodeId]++;
		sentCount++;
		print_clock();

		pthread_mutex_unlock(&pStateLock);
		if(i < n-1)
			sleep(minSendDelay);
	}

	pthread_mutex_lock(&pStateLock);
	pState = PASSIVE;
	printf("Node %d is PASSIVE\n", nodeId);
	pthread_mutex_unlock(&pStateLock);
	pthread_exit(NULL);
}

void make_active() {
	/* Check the state of the Node and then make the Node active by acquiring a lock on the state to avoid contention between multiple threads */
	if(neighborsCount > 0 && pState == PASSIVE && sentCount < maxNumber) {
		if(pthread_create(&outTId, NULL, send_app_msgs, NULL) < 0) 
			die("Start application thread failed");
		pState = ACTIVE;
		printf("Node %d is ACTIVE\n", nodeId);
	}
}

void start_snapshot() {
	/* Change the Snapshot state & send markers to all neighbors */
	pthread_mutex_lock(&pStateLock);

	printf("Starting Snapshot instance...\n");
	sState = IN_PROGRESS;
	pendingMarkers = neighborsCount;
	memcpy(localState, vectorClock, nodesCount * sizeof(int));

	MSG m;
	m.type = MARKER;
	m.from = nodeId;
	m.size = 0;
	for(int i=0; i<neighborsCount; i++) {
		neighbors[i].sState = IN_PROGRESS;
		neighbors[i].cState = 0;
		m.to = neighbors[i].id;
		sctp_sendmsg(neighbors[i].out, &m, sizeof(m), NULL, 0, 0, 0, 0 ,0, 0);
		print_msg(m);
	}

	pthread_mutex_unlock(&pStateLock);
}

void record_snapshot() {
	/* Record the local state */
	pthread_mutex_lock(&pStateLock);

	int i = nodesCount;
	localState[i++] = pState;
	for(; i<neighborsCount; i++)
		localState[i] = neighbors[i].cState;
	printf("Recording local state to file...\n");
	for(int i=0; i<nodesCount; i++)
		fprintf(outputFile, "%d ", localState[i]);
	fprintf(outputFile, "\n");

	if(nodeId != INITIATOR_NODE) {
		/* Forward the local state to the parent for Termination Detection */
		MSG m;
		m.type = SNAPSHOT;
		m.from = nodeId;
		m.to = -1; /* Converge cast to the Initiator Node */
		m.size = nodesCount + neighborsCount + 1;
		memcpy(m.buffer, localState, m.size * sizeof(int));
		sctp_sendmsg(neighbors[parent].out, &m, sizeof(m), NULL, 0, 0, 0, 0 ,0, 0);
		print_msg(m);

	} else {
		/* Copy the local state to the global state */
		memcpy(globalState[INITIATOR_NODE], localState, (nodesCount+neighborsCount+1) * sizeof(int));
	}

	pthread_mutex_unlock(&pStateLock);
}

void finish() {
	/* Change processing state & broadcasr Finish msgs */
	pthread_mutex_lock(&pStateLock);
	if(pState != DONE) {
		pState = DONE;

		MSG m;
		m.type = FINISH;
		m.from = -1; /* Broadcast from the Initiator Node */
		m.size = 0;
		printf("Terminating application & sending FINISH msgs...\n");
		for(int i=0; i<neighborsCount; i++) {
			m.to = neighbors[i].id;
			sctp_sendmsg(neighbors[i].out, &m, sizeof(m), NULL, 0, 0, 0, 0 ,0, 0);
			print_msg(m);
			close(neighbors[i].out);
		}
	}
	pthread_mutex_unlock(&pStateLock);
}

void check_consistency() {
	for(int i=0; i<nodesCount; i++) {
		for(int j=0; j<nodesCount; j++) {
			if(j!=i && globalState[j][i] > globalState[i][i]) {
				printf("Local state of Node %d: ", i);
				print_array(globalState[i], nodesCount);
				printf("Local state of Node %d: ", j);
				print_array(globalState[j], nodesCount);
				die("Snapshot inconsistent!");
			}
		}
	}
}

void detect_termination() {
	/* Detect termination of the MAP application protocol after all snapshots have been collected at the initiator using Chandy-Lamport protocol */
	printf("Checking global state for termination...\n");
	for(int i=0; i<nodesCount; i++){
		/* Node i should be Passive */
		if(globalState[i][nodesCount] != PASSIVE) {
			printf("Status of Node %d: %d\n", i, globalState[i][nodesCount]);
			return;
		}

		for(int j=nodesCount+1; j<2*nodesCount; j++) {
			/* Channel from neighbor j should be empty */
			if(globalState[i][j] != 0) {
				printf("Channel state between nodes %d & %d: %d\n", i, j, globalState[i][j]);
				return;
			}
		}
	}

	/* Application has terminated, cancel the snapshot thread */
	pthread_cancel(sTId);
	/* Broadcast Finish msgs */
	finish();
}

void *initiate_snapshot() {
	/* The snapshot initiator thread only initiates the Snapshot whenever the Initiator Node is Passive and it has been atleast 'snapshotDelay' ms since the previous initiation */
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	while(1) {
		printf("Sleeping for %.3f secs before next Snapshot...\n", snapshotDelay);
		sleep(snapshotDelay);

		pthread_mutex_lock(&sStateLock);
		if(sState == COMPLETED) {
			start_snapshot();
			pendingSnapshots = nodesCount - 1;
		}
		pthread_mutex_unlock(&sStateLock);
	}
}

void process_app_msg(MSG m, int fromIndex) {
	/* Increment the vector clock as per Fidge-Mattern's protocol */
	pthread_mutex_lock(&pStateLock);
	pthread_mutex_lock(&sStateLock);

	for(int j=0; j<nodesCount; j++)
		vectorClock[j] = MAX(vectorClock[j], m.buffer[j]);
	vectorClock[m.from]++;
	vectorClock[nodeId]++;
	print_clock();

	/* Activate the node according to MAP application protocol */
	make_active();

	/* If a snapshot is in progress record the channel state. No contention & no lock because only one receiver thread per neighbor */
	if(neighbors[fromIndex].sState == IN_PROGRESS)
		neighbors[fromIndex].cState++;

	pthread_mutex_unlock(&pStateLock);
	pthread_mutex_unlock(&sStateLock);
}

void process_marker_msg(MSG m, int fromIndex) {
	pthread_mutex_lock(&sStateLock);
	if(sState == COMPLETED)
		/* Start a new snapshot instance */
		start_snapshot();

	pendingMarkers--;
	neighbors[fromIndex].sState = COMPLETED;
	if(pendingMarkers == 0) {
		/* Complete the snapshot instance by recording the local state & sending it to the parent */
		sState = COMPLETED;
		record_snapshot();
	}
	pthread_mutex_unlock(&sStateLock);
}

void process_snaphot_msg(MSG m) {
	if(nodeId != INITIATOR_NODE) {
		/* Forward the Snapshot msg to the parent - ConvergeCast */
		sctp_sendmsg(neighbors[parent].out, &m, sizeof(m), NULL, 0, 0, 0, 0 ,0, 0);
		printf("Msg forwarded to parent Node %d: ", neighbors[parent].id);
		print_msg(m);

	} else {
		/* Copy the received local state to global state */
		pthread_mutex_lock(&sStateLock);
		memcpy(globalState[m.from], m.buffer, m.size * sizeof(int));
		pendingSnapshots--;
		if(pendingSnapshots == 0) {
			/* All local states received, run Termination Detection */
			check_consistency();
			detect_termination();
		}
		pthread_mutex_unlock(&sStateLock);
	}
}

void process_msgs(MSG m, int fromIndex) {
	/* Process a msg according to its type */
	switch(m.type) {
		case APPLICATION:
		process_app_msg(m, fromIndex);
		break;

		case MARKER:
		process_marker_msg(m, fromIndex);
		break;

		case SNAPSHOT:
		process_snaphot_msg(m);
		break;

		case FINISH:
		finish();
		break;
	}
}

void *recv_msgs(void *fromIndex) {
  	/* Receive incoming msgs on a given socket & process them */
	int i = (int) fromIndex;
	int flags, bytes;
	struct sctp_sndrcvinfo info;
	MSG m;
	char *error_msg;

	while((bytes = sctp_recvmsg(neighbors[i].in, &m, sizeof(m), NULL, 0, &info, &flags)) > 0) {
		print_msg(m);
		process_msgs(m, i);
	}
	if(bytes == 0)
		printf("Connection from Node %d closed\n", neighbors[i].id);
	if(bytes < 0) {
		sprintf(error_msg, "Receive from Node %d failed", neighbors[i].id);
		die(error_msg);
	}
	pthread_exit(NULL);
}

void *start_conns() {
	/* Create outgoing connections to all neighbors & activate the process if this node is the Initiator Node */
	MSG m;
	m.type = IDENTIFICATION;
	m.from = nodeId;
	m.size = 0;
	char *error_msg;

	for (int i=0; i<neighborsCount; i++) {
		/* Create SCTP stream socket (TCP-like) */
		int j = 9 + 3*i;
		neighbors[i].id = atoi(args[j+1]);
		neighbors[i].out = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);

		/* Connect to the neighbor */
		struct sockaddr_in outAddr;
		bzero(&outAddr, sizeof(outAddr));
		outAddr.sin_family = AF_INET;
		inet_aton(args[j+2], &outAddr.sin_addr);
		outAddr.sin_port = htons(atoi(args[j+3]));
		printf("Connecting to Node %d...\n", neighbors[i].id);
		if(connect(neighbors[i].out, (struct sockaddr *) &outAddr, sizeof(outAddr)) < 0) {
			sprintf(error_msg, "Connection to Node %s on %s:%s failed", args[j+1], args[j+2], args[j+3]);
			die(error_msg);
		}

		/* Send identification to neighbor */
		m.to = neighbors[i].id;
		sctp_sendmsg(neighbors[i].out, &m, sizeof(m), NULL, 0, 0, 0, 0 ,0, 0);
		print_msg(m);
	}

	pthread_exit(NULL);
}

void *listen_conns(void *arg) {
  	/* Listen for incoming connections from all neighbors & start connector thread on receiving the first incoming connection */
	int listenSock = (int) arg;
	int inSock, flags, bytes, j;
	struct sctp_sndrcvinfo info;
	MSG m;
	char *error_msg;

	for(int i=0; i<neighborsCount; i++) {
    	/* Accpet an incoming request */
		printf("Listening for connections...\n");
		inSock = accept(listenSock, NULL, NULL);
		if(inSock < 0)
			die("Accept failed");

		/* Identify the incoming connection */
		if((bytes = sctp_recvmsg(inSock, &m, sizeof(m), NULL, 0, &info, &flags)) <= 0 && m.type != IDENTIFICATION)
			die("Neighbor identification failed");
		printf("Connection accepted from Node %d\n", m.from);
		for(j=0; j<neighborsCount; j++) {
			if (m.from == neighbors[j].id)	{
				neighbors[j].in = inSock;
				break;
			}
		}
    		/* Start a separate thread to recevie messages on every incoming connection */
		if(pthread_create(&inTId[j], NULL, recv_msgs, (void *) j) < 0) {
			sprintf(error_msg, "Receive thread creation for Node %d failed", neighbors[j].id);
			die(error_msg);
		}

		if(i == 0 && nodeId != INITIATOR_NODE) {
			/* First incoming connection, set it as parent & start connector thread */
			parent = j;
			printf("Parent is Node %d\n", m.from);
			if(pthread_create(&cTId, NULL, start_conns, NULL) < 0)
				die("Start connector thread failed");
		}
	}

	if(nodeId == INITIATOR_NODE) {
		/* Make the Initiator node ACTIVE */
		printf("Sleeping for %d secs before becoming ACTIVE...\n", CONN_DELAY);
		sleep(CONN_DELAY);
		make_active();
		if(pthread_create(&sTId, NULL, initiate_snapshot, NULL) < 0)
			die("Start snapshot thread failed");
	}
	pthread_exit(NULL);
}

void create_conns(char *args[]) {
  	/* Create SCTP stream socket (TCP-like) to listen connections on and create listener & connector threads */
	int listenSock;
	struct sockaddr_in listenAddr;
	listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_SCTP);

  	/* Filter Incoming IP & Ports to accept connections from */
	bzero(&listenAddr, sizeof(listenAddr));
	listenAddr.sin_family = AF_INET;
	listenAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	listenAddr.sin_port = htons(port);

  	/* Bind the client socket */
	if(bind(listenSock, (struct sockaddr *) &listenAddr, sizeof(listenAddr)) < 0)
		die("Bind failed");

  	/* Start listening on given port */
	listen(listenSock, neighborsCount);
	if(pthread_create(&lTId, NULL, listen_conns, (void *) listenSock) < 0)
		die("Start listener thread failed");

	if(nodeId == INITIATOR_NODE) {
		/* Create connector thread for Node 0 to setup connections in a tree flow */
		printf("Sleeping for %d secs before starting connections...\n", CONN_DELAY);
		sleep(CONN_DELAY);
		if(pthread_create(&cTId, NULL, start_conns, NULL) < 0) {
			die("Start connector thread failed");
		}
	}
}

int main(int argc, char *argv[]) {
	/* Initialize the process parameters */
	args = argv;
	nodeId = atoi(argv[1]);
	port = atoi(argv[2]);
	nodesCount = atoi(argv[3]);
	minPerActive = atoi(argv[4]);
	maxPerActive = atoi(argv[5]);
	minSendDelay = (float) atoi(argv[6])/1000;
	snapshotDelay = (float) atoi(argv[7])/1000;
	maxNumber = atoi(argv[8]);
	neighborsCount = atoi(argv[9]);

	vectorClock = (int *) calloc(nodesCount, sizeof(int));
	localState = (int *) calloc(nodesCount + neighborsCount + 1, sizeof(int));
	globalState = (int **) calloc(nodesCount, sizeof(int *));
	if(nodeId == INITIATOR_NODE) {
		for(int i=0; i<nodesCount; i++)
			globalState[i] = (int *) calloc(2 * nodesCount, sizeof(int));
	}
	neighbors = (NODE*) calloc(neighborsCount, sizeof(NODE));
	inTId = (pthread_t*) calloc(neighborsCount, sizeof(pthread_t));
	for(int i=0; i<neighborsCount; i++)
		neighbors[i].id = atoi(argv[9 + 3*i + 1]);

	parent = -1;
	sentCount = 0;
	pendingMarkers = 0;
	pState = PASSIVE;
	sState = COMPLETED;
	outputFile = fopen(argv[9 + 3*neighborsCount + 1], "w");
	if(outputFile == NULL)
		die("Error opening Output file");

	srand(time(NULL));

	//pthread_mutexattr_t lockAttr;
	//pthread_mutexattr_init(&lockAttr);
	//pthread_mutexattr_settype(&lockAttr, PTHREAD_MUTEX_RECURSIVE);
	if(pthread_mutex_init(&pStateLock, NULL) != 0)
		die("ProcessStateLock init failed");
	if(pthread_mutex_init(&sStateLock, NULL) != 0)
		die("SnapshotStateLock init failed");

	/* Create incoming & outgoing connection threads */
	create_conns(argv);
	printf("Waiting for all connections to complete...\n");
	pthread_join(lTId, NULL);
	pthread_join(cTId, NULL);

	/* Wait for completion of all threads */
	printf("Waiting for all threads to complete...\n");
	for(int i=0; i<neighborsCount; i++)
		pthread_join(inTId[i], NULL);

	/* Cleanup */
	printf("Cleaning up...\n");
	fclose(outputFile);
	pthread_mutex_destroy(&pStateLock);
	pthread_mutex_destroy(&sStateLock);
	free(neighbors);
	free(inTId);
	free(vectorClock);
	free(localState);
	return 0;
}