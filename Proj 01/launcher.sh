#!/bin/bash
config="$1"
user="$2"
nodes=()
neighbors=()
i=0
# Read the file given in argument line by line
while IFS= read -r line || [ -n "$line" ]; do
	# Skip lines that do not start with an unsigned integer
	if [[ ! $line =~ ^[0-9]+ ]]; then
		continue
	fi

	# Truncate comments after '#' and leading white spaces
	tokens=($(echo ${line%%#*} | sed -e 's/^ *//g;s/ *$//g'))

	# Read config parameters
	if [[ i -eq 0 ]]; then
		global_info=("${tokens[@]}")
	elif [[ i -le $global_info ]]; then
		tmp=($(host ${tokens[1]}.utdallas.edu))
		tokens[1]=${tmp[3]}
		nodes[(($tokens))]="${tokens[@]}"
	elif [[ i -gt $global_info ]]; then
		neighbors[(($i-$global_info-1))]="${tokens[@]}"
	fi
	((i+=1))
done < "$config"

bin_dir=bin
bin_file=map
config=${config%%.txt}
mkdir $bin_dir
gcc map.c -o $bin_dir/$bin_file -std=c99 -pthread -lsctp -D_BSD_SOURCE
for (( i=${#nodes[@]}-1; i>=0; i-- )); do
	# Copy the compiled binary to all nodes 
	node=(${nodes[$i]})
	echo Launching Node ${node[0]} on ${node[1]} as $user...
	scp $bin_dir/$bin_file $user@${node[1]}:.

	# Start execution of all nodes
	neighbors_count=0
	neighbors_info=()
	for neighbor in ${neighbors[${node[0]}]}; do
		neighbors_info[(($neighbors_count))]="${nodes[$neighbor]}"
		((neighbors_count+=1))
	done
	if [[ i -gt 0 ]]; then
		# Run normal nodes in background '&'
		ssh $user@${node[1]} "nohup ./$bin_file ${node[0]} ${node[2]} ${global_info[@]} $neighbors_count ${neighbors_info[@]} $config-${node[0]}.out > $config-${node[0]}.log 2>&1 &"
	else
		# Run the Initiator Node in forground to know termination of the processes
		ssh $user@${node[1]} "nohup ./$bin_file ${node[0]} ${node[2]} ${global_info[@]} $neighbors_count ${neighbors_info[@]} $config-${node[0]}.out > $config-${node[0]}.log 2>&1"
	fi
	echo
done
