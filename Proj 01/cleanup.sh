#!/bin/bash
config="$1"
user="$2"
nodes=()
i=0
# Read the file given in argument line by line
while IFS= read -r line || [ -n "$line" ]; do
	# Skip lines that do not start with an unsigned integer
	if [[ ! $line =~ ^[0-9]+ ]]; then
		continue
	fi

	# Truncate comments after '#' and leading white spaces
	line=$(echo ${line%%#*} | sed -e 's/^ *//g;s/ *$//g')
	tokens=($line)

	# Read config parameters
	if [[ i -eq 0 ]]; then
		global_info=("${tokens[@]}")
	elif [[ i -le $global_info ]]; then
		tokens[1]=${tokens[1]}.utdallas.edu
		nodes[(($tokens))]="${tokens[@]}"
	fi
	((i+=1))
done < "$config"

output_dir=output
bin_file=map
config=${config%%.txt}
mkdir $output_dir
for node in "${nodes[@]}"; do
	# Copy all output files & delete the remote directory 
	node=($node)
	echo Cleaning up Node ${node[0]} on ${node[1]}...
	ssh $user@${node[1]} "pkill -u $user $bin_file"
	scp $user@${node[1]}:./$config-${node[0]}.* $output_dir/.
	ssh $user@${node[1]} "rm -f $bin_file $config-${node[0]}.* $output_dir/."
	echo
done
